<a href='https://gitee.com/IFLY-DevNet/net-axe/stargazers'><img src='https://gitee.com/IFLY-DevNet/net-axe/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/IFLY-DevNet/net-axe/members'><img src='https://gitee.com/IFLY-DevNet/net-axe/badge/fork.svg?theme=white' alt='fork'></img></a>

[![IFLY-DevNet/NetAxe](https://gitee.com/IFLY-DevNet/net-axe/widgets/widget_card.svg?colors=2877c7,e0e0e0,bddcff,e3e9ed,666666,9b9b9b)](https://gitee.com/IFLY-DevNet/net-axe)

## 🌟 介绍

网络自动化平台
功能:

1. 资产管理
2. 配置备份(nornir)
3. 配置差异比较
4. webssh
5. 设备数据的统一采集并统一数据格式(celery 多进程+netmiko)
6. 设备接口利用率分析

## 平台截图

1. 登录页  
   ![image](resource/login.jpg)
2. 资产管理
   ![image](resource/asset.jpg)
3. 差异比较
   ![image](resource/git-diff.jpg)

## 安装教程

安装前置条件
操作系统 : centos 7.×  
docker 版本 >= 18.9  
docker-compose 版本 >= 1.18.0

### 直接运行方式(适用于 amd64 系统，在 centos7 上验证通过)

1. 更新 docker 配置文件
 <pre><code>
   sudo tee /etc/docker/daemon.json <<-'EOF'
   {
   "registry-mirrors": ["https://tawedu6l.mirror.aliyuncs.com"]
   }
   EOF
   sudo systemctl daemon-reload
   sudo systemctl restart docker
 </code></pre>
2. 进入到 docker 目录下 先启动数据库
 <pre><code>
   cd docker/databases  
   docker-compose up -d
 </code></pre>
3. 进入到 server 目录下，启动服务
 <pre><code>
   cd docker/server  
   docker-compose up netaxe-server -d
 </code></pre>
4. 数据初始化(docker/server 路径下)
进入后端服务容器命令行
 <pre><code>
   docker exec -it netaxe-server /bin/bash  
   python3 manage.py migrate  
   python3 manage.py init_asset  
   exit
 </code></pre>
5. 重新编排服务(docker/server 路径下)
 <pre><code>
   关闭该路径下所有容器服务
   docker-compose down -v  
   编辑容器启动文件  
   vi docker-compsoe.yml  
   将 netaxe-server 容器的 command 命令改为 sh start.sh web  
   command: sh start.sh web 将这一行放开  
   #command: sleep 999999 将这一行注释
 </code></pre>
6. 重新启动后端服务(docker/server 路径下)
 <pre><code>
   docker-compose up -d
 </code></pre>

### 本地构建方式

1. 克隆项目到本地  
   git clone https://gitee.com/IFLY-DevNet/net-axe.git
2. 进入项目目录  
   cd net-axe
3. 打包后端镜像  
   cd netaxe  
   wget http://npm.taobao.org/mirrors/python/3.9.11/Python-3.9.11.tgz  
   docker build -t netaxe-backend:latest .
4. 打包前端镜像  
   cd web  
   docker build -t netaxe-web:latest .

## 软件架构

软件架构说明

### 🚀 前端 vue admin work

环境准备
该项目本地环境需要安装

##### Node 版本: v16.13.1

##### npm 版本: v6.14.5

##### Git 版本: v2.23.0

### 后端 Django + celery

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

# 💖 感谢伟大的[Django](https://github.com/django/django)、[VUE](https://github.com/vuejs/vue)、[vue-admin-work](https://github.com/qingqingxuan/vue-admin-work)
