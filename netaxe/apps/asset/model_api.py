# -*- coding: utf-8 -*-
# @Time    : 2020/9/20 22:30
# @Author  : LiJiaMin
# @Site    :
# @File    : model_api.py
# @Software: PyCharm
from collections import OrderedDict
from django.db import connections
from apps.asset.models import NetworkDevice


# 获取登录网络设备所需相关信息
def get_device_info_v2(**kwargs):
    support_vendor = [
        'H3C', 'Huawei', 'Ruijie', 'Maipu', 'Hillstone', 'Mellanox', 'centec'
    ]
    if kwargs:
        kwargs['status'] = 0
        kwargs['auto_enable'] = True
        all_devs = NetworkDevice.objects.filter(**kwargs).select_related(
            'idc_model', 'model', 'role', 'attribute', 'category', 'vendor', 'idc', 'framework', 'plan', 'netzone',
            'rack').prefetch_related('bgbu', 'bind_ip', 'adpp_device').values(
            'id', 'serial_num', 'manage_ip', 'name', 'vendor__name', 'soft_version', 'vendor__alias',
            'category__name', 'framework__name', 'model__name',
            'patch_version', 'soft_version', 'status', 'idc__name', 'auto_enable',
            'ha_status', 'chassis', 'slot', 'bind_ip__ipaddr')
    else:
        # 获取所有cmdb设备
        all_devs = NetworkDevice.objects.filter(
            status=0, vendor__alias__in=support_vendor, auto_enable=True).select_related(
            'idc_model', 'model', 'role', 'attribute', 'category', 'vendor', 'idc', 'framework', 'netzone',
            'rack').prefetch_related('bind_ip', 'account').values(
            'id', 'serial_num', 'manage_ip', 'name', 'soft_version', 'vendor__name', 'vendor__alias',
            'category__name', 'framework__name', 'model__name',
            'patch_version', 'soft_version', 'status', 'idc__name', 'auto_enable',
            'ha_status', 'chassis', 'slot', 'bind_ip__ipaddr')
    for dev in all_devs:
        dev['account'] = [x for x in dev.account.all()]
    result = OrderedDict()
    for item in all_devs:
        result.setdefault(item['manage_ip'], {**item})
    connections.close_all()
    return list(result.values())
